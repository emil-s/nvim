local lsp_settings = {
    ui = {
        icons = {
            server_installed = "✓",
            server_pending = "➜",
            server_uninstalled = "✗",
        },
        keymaps = {
            toggle_server_expand = "<CR>",
            install_server = "i",
            update_server = "u",
            uninstall_server = "X",
        },
    },

    pip = {
        install_args = {},
    },
    log_level = vim.log.levels.INFO,
    allow_federated_servers = true,
    max_concurrent_installers = 4,
}


local lsp_installer = require("nvim-lsp-installer")

require'lspconfig'.rnix.setup{}

lsp_installer.on_server_ready(function(server)
    local opts = {lsp_settings}
    server:setup(opts)
    vim.cmd [[ do User LspAttachBuffers ]]
end)
