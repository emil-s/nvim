local o = vim.o
local g = vim.g
local wo = vim.wo
local bo = vim.bo
local fn = vim.fn
local cmd = vim.cmd


g.loaded_python_provider   = 0
g.loaded_python3_provider  = 0
g.python_host_skip_check   = 1
g.python3_host_skip_check  = 1
g.loaded_node_provider     = 0
g.loaded_ruby_provider     = 0
g.loaded_perl_provider     = 0

g.loaded_tutor             = 1
g.loaded_spec              = 1
g.loaded_2html_plugin      = 1
g.loaded_getscript         = 1
g.loaded_getscriptPlugin   = 1
g.loaded_gzip              = 1
g.loaded_logipat           = 1
g.loaded_logiPat           = 1
g.loaded_matchparen        = 1
g.loaded_netrw             = 1
g.loaded_netrwFileHandlers = 1
g.loaded_netrwPlugin       = 1
g.loaded_netrwSettings     = 1
g.loaded_rrhelper          = 1
g.loaded_spellfile_plugin  = 1
g.loaded_sql_completion    = 1
g.loaded_syntax_completion = 1
g.loaded_tar               = 1
g.loaded_tarPlugin         = 1
g.loaded_vimball           = 1
g.loaded_vimballPlugin     = 1
g.loaded_zip               = 1
g.loaded_zipPlugin         = 1
g.vimsyn_embed             = 1


-- leader
g.mapleader = " "
g.maplocalleader = " "

-- indentation
o.expandtab = false
o.shiftwidth = 4
o.shiftwidth = 4
o.smartindent = true
o.smartindent = true
o.cindent = true
o.tabstop = 4
o.tabstop = 4
o.smarttab = true
o.softtabstop = 4
o.softtabstop = 4
o.shiftround = true
o.breakindent = true
o.joinspaces = false

-- paren
g.showmatch = true
g.matchtime = 2

-- line numbers
o.number = true
o.relativenumber = true
o.numberwidth = 2

-- completion
o.completeopt='menu,menuone,noselect'

-- visuals
-- o.wrap = false
o.termguicolors = true
o.updatetime = 500
o.redrawtime = 100

-- grepping
o.grepprg = "rg --vimgrep"

--scrolloff
o.scrolloff = 8
o.sidescrolloff = 12

-- folding
o.foldlevel = 1
o.foldnestmax = 3
o.foldminlines = 1
o.foldmethod = "expr"
o.foldexpr = "nvim_treesitter#foldecpr()"

-- title
o.title = true
o.titlelen = 24
o.titlestring = "%t"

-- completion
o.pumheight = 10
o.pumwidth = 16
o.pumblend = 17
o.omnifunc = 'syntaxcomplete#Complete'

-- mouse
o.mouse = "a"

-- bell
o.belloff = "all"

-- clipboard
o.clipboard = "unnamedplus"

-- statusline
o.showmode = false
g.showmode = false
o.modelines = 0
