require('impatient')
require('impatient').enable_profile()

vim.g.did_load_filetypes = 1

vim.cmd([[
	syntax off
	filetype plugin indent off
]])

vim.opt.shadafile = "NONE"

require('core')
require('packages')

vim.opt.shadafile = ""

vim.defer_fn(function()
	vim.cmd([[
	syntax on
	filetype plugin indent on
	PackerLoad nvim-treesitter
	]])
	require('core.keybindings')
end, 10)
