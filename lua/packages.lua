vim.cmd('packadd packer.nvim')

require('packer').init({
	compile_path = vim.fn.stdpath('config') .. '/lua/core/packer_compiled.lua',
	display = {
		title = 'Packer',
	},
	git = {
		clone_timeout = 300,
	},
})

require('packer').startup({
	function()

		-- base
		use {'wbthomason/packer.nvim', opt = true}


		use	'lewis6991/impatient.nvim'


		use 'nathom/filetype.nvim'

		
		use {
			'rose-pine/neovim', as = 'rose-pine',
			config = function()
				vim.g.rose_pine_variant = 'base'
				vim.g.rose_pine_disable_italics = true
				vim.cmd('colorscheme rose-pine')
			end
		}

		use {
			'marko-cerovac/material.nvim',
			config = function()
				require('packages.material')
			end,
		}


		-- modeline
		use {
			'famiu/feline.nvim', -- branch = 'develop',
			requires = 'kyazdani42/nvim-web-devicons',
			config = function()
				require('packages/feline')
			end,
		}

		
		-- greeter
		use {
			'goolord/alpha-nvim',
			config = function ()
				require'alpha'.setup(require'alpha.themes.dashboard'.opts)
			end,
		}


		-- syntax highlighting
		use {
			'nvim-treesitter/nvim-treesitter', opt = true,
			run = ':TSUpdate',
			config = function()
				require('packages.nvim-treesitter')
			end,
			requires = {
				'p00f/nvim-ts-rainbow',
				'windwp/nvim-ts-autotag'}
		}


		-- editing
		use {
			'numToStr/Comment.nvim',
			event = 'BufRead',
			config = function()
				require('Comment').setup()
			end,
		}


		-- file explorer
		use {
			'kyazdani42/nvim-tree.lua',
			requires = 'kyazdani42/nvim-web-devicons',
			config = function()
				require'nvim-tree'.setup()
			end,
		}


		-- fuzzy finder
		use {
			'nvim-telescope/telescope.nvim',
			cmd = 'Telescope',
			config = function()
				require('packages.telescope')
			end,
			requires = 'nvim-lua/plenary.nvim'
		}


		-- lsp
		use {
			'neovim/nvim-lspconfig',
			ft = {'shell', 'rust', 'python', 'cs', 'cpp', 'css', 'html', 'lua'},
			requires = {
				'jose-elias-alvarez/null-ls.nvim',
				'williamboman/nvim-lsp-installer',
				{'weilbith/nvim-code-action-menu',
					cmd = 'CodeActionMenu'}},
			config = function()
				require('packages/lsp')
			end
		}


		-- completion
		use {
			'hrsh7th/nvim-cmp',
			event = 'InsertEnter',
			after = {'nvim-lspconfig', 'LuaSnip'},
			requires =  {
				{'onsails/lspkind-nvim', opt = true},
				{'hrsh7th/cmp-buffer', after = 'nvim-cmp', opt = true},
				{'hrsh7th/cmp-path', after = 'nvim-cmp', opt = true},
				{'hrsh7th/cmp-nvim-lua', after = 'nvim-cmp', opt = true},
				{'hrsh7th/cmp-nvim-lsp', after = 'nvim-cmp', opt = true}
			},
			config = function()
				require ('packages/nvim-cmp')
			end
		}


		-- snippets
		use {
			'L3MON4D3/LuaSnip', event = 'InsertEnter',
			requires = 'rafamadriz/friendly-snippets'
		}

		use {
			'windwp/nvim-autopairs', event = 'InsertCharPre',
			config = function()
				require('packages.autopairs')
			end
		}

		--motion
		use {'ggandor/lightspeed.nvim',
			event = 'BufRead'
		}


	end,
})

require('core.packer_compiled')
